# Starter App

Please build a small web application that:

* Allows a user to register. A user has the following properties:
    * email (must be unique)
    * password
    * fullname

* Search other users by fullname and add them as friends
* Allows the user to login
* Allows the user to change/update their fullname 
* Allows user to view his friends and remove any of them

Ensure that the application includes at least the following elements:

* A web frontend. You may use the HTML, CSS, and JavaScript framework(s) of your choice, or none at all.
* A server backend built on Node.js. Please write your Node.js code in JavaScript only and avoid the use of languages such as CoffeeScript or TypeScript.
* A data store using Redis (Heroku Redis free tier) or MongoDB

Host the application on Heroku (free tier).

All code must be on a public fork of this repository on GitHub or Bitbucket (or any other public git repository hosting).

When you have completed this exercise, please send an email with confirmation and links to the application and repository.

